# CleanKinja #
CleanKinja is a Firefox extension that provides a more pure reading experience to the Gawker blog network by adding a degree of separation between all the Gawker sister sites.
 
With the extension installed, each main Gawker site is insulated from each other. Cross-posted content and shared link bundles from other Gawker blogs are removed from the current blog feed to provide a more single-interest experience.  For example, when reading Kotaku no stories or shared posts from Gizmodo will appear in the blog feed and vice-versa.
 
The extension can be downloaded and installed here: https://addons.mozilla.org/en-us/firefox/addon/cleankinja/