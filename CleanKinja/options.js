function saveOptions(e) {
  e.preventDefault();

  browser.storage.local.set({
    curationheaderoff: document.querySelector("#curationheaderoff").checked
  });
  //console.log('CLEANKINJA: Changing to: ' + document.querySelector("#curationheaderoff").checked);
}

function restoreOptions() {

  function setCurrentChoice(result) {
    if(result[0]) {
      document.querySelector("#curationheaderoff").checked = result[0]["curationheaderoff"] || false;
    } else {
      document.querySelector("#curationheaderoff").checked = result.curationheaderoff || false;
    }
    //console.log('CLEANKINJA: currently: ' + result.curationheaderoff);
    //alert('CLEANKINJA: currently: ' + result.curationheaderoff);
  }

  function onError(error) {
    console.log(`CLEANKINJA: Error: ${error}`);
    //alert(`CLEANKINJA: Error: ${error}`);
  }

  var getting = browser.storage.local.get("curationheaderoff");
  getting.then(setCurrentChoice, onError);
}

document.addEventListener("DOMContentLoaded", restoreOptions);
document.querySelector("#curationheaderoff").addEventListener("change", saveOptions);
