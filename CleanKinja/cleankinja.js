//filter.js - Where the magic happens...
//Filters out gawker sister site content - only shows content hosted directly on the current site - no linked content or crossposting
//run "web-ext run -p default --browser-console --start-url about:addons"


//Gets current Gawker host site
var currentSite = window.location.host;
var gawkerSiteList = [
		"kotaku.com",
		"gawker.com",
		"gizmodo.com",
		"jezebel.com",
		"deadspin.com",
		"lifehacker.com",
		"jalopnik.com",
		"theroot.com",
		"splinternews.com",
		"avclub.com",
		"earther.com",
		"thetakeout.com",
		"theonion.com",
		"clickhole.com",
		"theinventory.com",
		"fusion.kinja.com",
		"kinja.com"
	];

//Removes host site from blocklist to allow content only from host
for(var i = 0; i < gawkerSiteList.length; i++) {
	if(currentSite.indexOf(gawkerSiteList[i]) > -1) {
		currentSite = gawkerSiteList[i];
	}
	if(currentSite == gawkerSiteList[i]) {
		gawkerSiteList.splice(i, 1);
	}
}

//Removes trending sidebar on core site
var sidebar = document.getElementById("sidebar_wrapper");
if(sidebar != null) {
	hideElement(sidebar);
	//console.log("cleanKinja - removing Trending sidebar");
}

//Removes trending sidebar on story pages
var trending = document.getElementsByClassName("sidebar");
for (var i = 0; i < trending.length; i++) {
	hideElement(trending[i]);
	//console.log("cleanKinja - removing Trending sidebar");
}

//Curation Header removal
function onError(error) {
  //alert("Error on curation header fetch");
  console.log(`CLEANKINJA: Error: ${error}`);
}

function onHeaderGot(item) {
  var curationheaderoff = false;

  //bug with versions below 52 that returns a dict like [{"curationheaderoff":true}]
  if (item[0]) {
	curationheaderoff = item[0]["curationheaderoff"] || false;
  } else {
    curationheaderoff = item.curationheaderoff || false;;
  }

  if (curationheaderoff) {
  	var curationHeader = document.getElementsByClassName("curation-mountain");
	for(var i = 0; i < curationHeader.length; i++) {
		hideElement(curationHeader[i])
		//console.log("CLEANKINJA: cleanKinja - removing Curation Header from top of blog");
	}
  }
}

var getHeaderOption = browser.storage.local.get("curationheaderoff");
getHeaderOption.then(onHeaderGot, onError);


//Re-centers main blog content - looks much better after link side-bar is removed
var trending = document.getElementsByClassName("sidebar");
for (var i = 0; i < trending.length; i++) {
	trending[i].style.width = "25%";
}

//Crosspost and link bundle removal
var posts = Array.prototype.slice.call(document.getElementsByClassName("js_post_item"));
for (var i = 0; i < posts.length; i++) {
	var links = posts[i].getElementsByClassName("js_link");
	linkloop:
	for (var j = 0; j < links.length; j++) {
		for(var k = 0; k < gawkerSiteList.length; k++) {
			if(links[j].href.indexOf(gawkerSiteList[k]) > -1) {
				if(links[j].hasAttribute("data-ga") && ((links[j].getAttribute("data-ga").indexOf("stream post click") > -1 ) || (links[j].getAttribute("data-ga").indexOf("Kinja Roundup") > -1 ))) {
					hideElement(posts[i]);
					//console.log("Hiding post to " + links[j].href);
					break linkloop;
				}
			}
		}
	}
}

//Sponsored post hosted on main page removal - finds posts with inner class "meta__label meta__label--sponsored"
//Doesn't work because sponsored content is loaded later...
var sponsored = document.getElementsByClassName("meta__label meta__label--sponsored");
for (var i = 0; i < sponsored.length; i++) {
	console.log("CleanKinja - removing sponsored post hosted on main " + i);
	var element = sponsored[i];
	while(element.className != "post-wrapper js_post-wrapper " && element !== window.top.document && element != null) {
		element = element.parentNode;
	}
	if(element !== window.top.document && element != null) {
		hideElement(element);
	}
}

//Footer ads removal
var footer = document.getElementById("taboola-below-article-thumbnails");
if(footer != null) {
	hideElement(footer);
	//console.log("cleanKinja - removing taboola footer links");
}

//Filters the "More by --- " links at the end of the site (the main sites and most of the sub-sites, at least)
//Works on all sites (tested gizmodo, kotaku, gawker, lifehacker)
var footerLinks = document.getElementsByClassName("thumb-inset js_inset js_inset_thumb");
for (var i = 0; i < footerLinks.length; i++) {
	var linked = footerLinks[i].getElementsByTagName("a");
	if(linked != null && linked.length > 0) {
		for(var j = 0; j < gawkerSiteList.length; j++) {
			if(linked[0].href.indexOf(gawkerSiteList[j]) > -1) {
				hideElement(footerLinks[i]);
				//console.log("cleanKinja - removing More by... footer link to " + gawkerSiteList[j]);
			}
		}
	}
}

var footerLinksV2 = document.getElementsByClassName("content__container");
for (var i = 0; i < footerLinksV2.length; i++) {
	var linked = footerLinks[i].getElementsByTagName("a");
	if(linked != null && linked.length > 0) {
		for(var j = 0; j < gawkerSiteList.length; j++) {
			if(linked[0].href.indexOf(gawkerSiteList[j]) > -1) {
				hideElement(footerLinks[i]);
				//console.log("cleanKinja - removing footer link to " + gawkerSiteList[j]);
			}
		}
	}
}

//Filters the sidebar links on full-size feature articles
var sidebarLinks = document.getElementsByClassName("sidebar_wrapper");
for (var i = 0; i < sidebarLinks.length; i++) {
	var linked = sidebarLinks[i].getElementsByTagName("a");
	if(linked != null && linked.length > 0) {
		for(var j = 0; j < gawkerSiteList.length; j++) {
			if(linked[0].href.indexOf(gawkerSiteList[j]) > -1) {
				hideElement(sidebarLinks[i]);
			}
		}
	}
}

function hideElement(element) {
	element.style.position = "absolute";
	element.style.left = "0px";
	element.style.left = "0px";
	element.style.opacity = "0";
	element.style.pointerEvents = "none";
}
